package unit.services;

import model.entities.User;
import model.repositories.UserRepository;
import model.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class UserServiceTest {

	private UserService userService;
	private UserRepository userRepository;

	@Before
	public void setUp() throws Exception {
		userRepository = Mockito.mock(UserRepository.class);
		userService = new UserService(userRepository);
	}

	@Test
	public void testCreate() {
		User user = new User();
		userService.create(user);
		Mockito.verify(userRepository).create(user);
	}

	@Test
	public void testUpdate() {
		User user = new User("dummy", "123", "dummy@dummy.com");
		userService.update(user.userName, user);
		Mockito.verify(userRepository).update(user);
	}

	@Test
	public void testDelete() {
		String userName = "userName";
		userService.delete(userName);
		Mockito.verify(userRepository).delete(userName);
	}

	@Test
	public void testCheckPassword() {
		User user = new User();
		userService.checkPassword(user);
		Mockito.verify(userRepository).checkPassword(user);
	}

	@Test
	public void testGetByUsername() {
		String userName = "userName";
		userService.getByUsername(userName);
		Mockito.verify(userRepository).getByUsername(userName);
	}
}
