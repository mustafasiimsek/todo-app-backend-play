package unit.services;

import model.entities.Note;
import model.repositories.NoteRepository;
import model.services.NoteService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class NoteServiceTest {

	private NoteService noteService;
	private NoteRepository noteRepository;

	@Before
	public void setUp() {
		noteRepository = Mockito.mock(NoteRepository.class);
		noteService = new NoteService(noteRepository);
	}

	@Test
	public void testCreate() {
		String userName = "userName";
		Note note = new Note();
		noteService.create(userName, note);
		Mockito.verify(noteRepository).create(userName, note);
	}

	@Test
	public void testDelete() {
		Long id = null;
		noteService.delete(id);
		Mockito.verify(noteRepository).delete(id);
	}

	@Test
	public void testGetAllNote() {
		String userName = "userName";
		int page = 1;
		noteService.getAllNote(userName, page);
		Mockito.verify(noteRepository).getAllNote(userName, page);
	}

	@Test
	public void testUpdate() {
		Long id = null;
		Note note = new Note();
		noteService.update(id, note);
		Mockito.verify(noteRepository).update(note);
	}

	@Test
	public void testGetById() {
		Long id = null;
		noteService.getById(id);
		Mockito.verify(noteRepository).getById(id);
	}
}
