package unit.controllers;

import model.entities.User;
import org.junit.Test;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import play.libs.Json;

import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;

import static org.junit.Assert.assertEquals;
import static play.test.Helpers.*;


public class HomeControllerTest extends WithApplication {

	@Test
	public void testWelcomeUser() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			// put test stuff
			// put asserts
//           User user = new User();
//           user.userName = "ms";
//           user.email ="";
//           Json.toJson(user);
			Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
					.method(GET)
					.session("identifier", "ms")
					.uri("/a/home");
			Result result = route(requestBuilder);
			assertEquals(OK, result.status());
			assertTrue(contentAsString(result).contains("Welcome ms"));
		});
	}

	@Test
	public void testWellcome() {
		Helpers.running(Helpers.fakeApplication(), () -> {
			Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
					.method(GET)
					.uri("/a/home");
			Result result = route(requestBuilder);
			assertEquals(OK, result.status());
			assertTrue(contentAsString(result).contains("Welcome"));
		});
	}

}