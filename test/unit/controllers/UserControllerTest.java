package unit.controllers;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import controllers.UserController;
import model.entities.User;
import model.repositories.UserRepository;
import model.services.UserService;
import org.junit.Before;
import org.junit.Test;


import org.mockito.ArgumentCaptor;
import play.libs.Json;
import play.mvc.Http;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static play.mvc.Results.redirect;


public class UserControllerTest extends BaseControllerTest {

	private UserController userController;
	private UserService userServiceMock;

	@Before
	public void setUp() throws Exception {
		userServiceMock = mock(UserService.class);
		userController = new UserController(userServiceMock);
	}

	@Test
	public void shouldDeleteUser() throws Exception {
		setCurrentContext(dummyUser(), new Http.RequestBuilder());
		userController.delete();
		verify(userServiceMock).delete(dummyUser().userName);
	}

	@Test
	public void shouldInfo() throws Exception {
		setCurrentContext(dummyUser(), new Http.RequestBuilder());
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		userController.info();
		verify(userServiceMock).getByUsername(dummyUser().userName);
	}

	@Test
	public void shouldLogout() throws Exception {
		setCurrentContext(dummyUser(), new Http.RequestBuilder());
		userController.logout();
		assertEquals(null, Http.Context.current.get().session().get("identifier"));
	}

	@Test
	public void shouldLogin() throws Exception {
		setCurrentContext(dummyUser(), new Http.RequestBuilder()
				.bodyJson(Json.toJson(dummyUser())));
		when(userServiceMock.checkPassword(dummyUser())).thenReturn(dummyUser().userName);
		userController.login();
		assertEquals(dummyUser().userName, Http.Context.current.get().session().get("identifier"));
	}

	@Test
	public void shouldNotLogin() throws Exception {
		setCurrentContext(dummyUser(), new Http.RequestBuilder()
				.bodyJson(Json.toJson(dummyUser())));
		when(userServiceMock.checkPassword(dummyUser())).thenReturn(null);
		assertEquals(redirect("/a/users/login").redirectLocation(),
				userController.login().redirectLocation());
	}

	@Test
	public void shouldCreateUser() throws Exception {
		//ArgumentCaptor
		ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
		when(userServiceMock.create(userArgumentCaptor.capture())).thenReturn(dummyUser());

		Http.RequestBuilder request = new Http.RequestBuilder()
				.bodyJson(Json.toJson(dummyUser()));
		Http.Context context = new Http.Context(request);
		Http.Context.current.set(context);
		userController.create();
		assertEquals(userArgumentCaptor.getValue().userName, dummyUser().userName);
	}

	@Test
	public void shouldUpdate() throws Exception {
		User dummyUpdate = new User("dummy", "456", "test@test.com");
		setCurrentContext(dummyUser(), new Http.RequestBuilder()
				.bodyJson(Json.toJson(dummyUpdate)));
		ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
		when(userServiceMock.update(eq(dummyUser().userName), userArgumentCaptor.capture()))
				.thenReturn(dummyUpdate);
		userController.update();
		assertEquals(userArgumentCaptor.getValue().email, dummyUpdate.email);
	}
}
