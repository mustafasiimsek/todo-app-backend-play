package unit.controllers;

import controllers.NoteController;
import model.entities.Note;
import model.entities.User;
import model.services.NoteService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Http;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static play.mvc.Http.Status.OK;


import static org.mockito.Mockito.mock;
import static play.mvc.Http.Status.UNAUTHORIZED;
import static play.test.Helpers.PUT;

public class NoteControllerTest extends BaseControllerTest {

	private NoteController noteController;
	private NoteService noteServiceMock;

	private Note dummyNote = null;

	private Note dummyNote() {
		if (this.dummyNote == null) {
			this.dummyNote = new Note(1l, "dummy", dummyUser());
			return dummyNote;
		} else {
			return this.dummyNote;
		}
	}

	@Before
	public void setUp() throws Exception {
		noteServiceMock = mock(NoteService.class);
		noteController = new NoteController(noteServiceMock);
	}


	@Test
	public void shouldCreate() throws Exception {
		setCurrentContext(dummyNote().user, new Http.RequestBuilder().bodyJson(Json.toJson(dummyNote())));
		ArgumentCaptor<Note> noteArgumentCaptor = ArgumentCaptor.forClass(Note.class);
		when(noteServiceMock.create(eq(dummyNote().user.userName), noteArgumentCaptor.capture()))
				.thenReturn(dummyNote());
		noteController.create();
		assertEquals(noteArgumentCaptor.getValue().id, dummyNote().id);
	}

	@Test
	public void shouldDelete() throws Exception {
		setCurrentContext(dummyNote().user, new Http.RequestBuilder());
		Mockito.when(noteServiceMock.getById(1L)).thenReturn(dummyNote());
		assertEquals(OK, noteController.delete(1l).status());
	}

	@Test
	public void shouldNotDelete() throws Exception {
		User notDummy = new User("notDummy", "456", "not@dummy.com");
		setCurrentContext(notDummy, new Http.RequestBuilder());
		when(noteServiceMock.getById(dummyNote().id)).thenReturn(dummyNote());
		assertEquals(UNAUTHORIZED, noteController.delete(1L).status());
	}

	@Test
	public void shouldUpdate() throws Exception {
		Note newDummyNote = new Note(null, "new dummy", dummyUser());
		setCurrentContext(dummyNote().user, new Http.RequestBuilder()
				.bodyJson(Json.toJson(newDummyNote)));
		ArgumentCaptor<Note> noteArgumentCaptor = ArgumentCaptor.forClass(Note.class);
		when(noteServiceMock.getById(dummyNote().id)).thenReturn(dummyNote());
		when(noteServiceMock.update(eq(dummyNote().id), noteArgumentCaptor.capture()))
				.thenReturn(newDummyNote);
		noteController.update(dummyNote().id);
		assertEquals(newDummyNote.note, noteArgumentCaptor.getValue().note);
	}

	@Test
	public void shouldNotUpdate() throws Exception {
		Http.RequestBuilder request = new Http.RequestBuilder()
				.method(PUT)
				.bodyJson(Json.toJson(dummyNote()));
		Http.Context context = new Http.Context(request);
		context
				.session().put("identifier", "notDummy");
		Http.Context.current.set(context);
		when(noteServiceMock.getById(dummyNote().id)).thenReturn(dummyNote());
		noteController.update(dummyNote().id);
		assertEquals(UNAUTHORIZED, noteController.update(dummyNote().id).status());

	}

	@Test
	public void shouldGetAllNote() throws Exception {
		List<Note> noteList = new ArrayList<Note>();
		noteList.add(dummyNote());
		setCurrentContext(dummyNote().user, new Http.RequestBuilder()
				.uri("/a/users/notes?page=1"));
		when(noteServiceMock.getAllNote(dummyNote().user.userName, 1))
				.thenReturn(noteList);
		noteController.getAllNote();
		verify(noteServiceMock).getAllNote(dummyNote().user.userName, 1);
	}

	@Test
	public void ShouldNotGetAllNote() throws Exception {
		setCurrentContext(dummyNote().user, new Http.RequestBuilder()
				.uri("/a/users/notes?page=0"));
		assertEquals(UNAUTHORIZED, noteController.getAllNote().status());

	}
}