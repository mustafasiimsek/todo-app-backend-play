package unit.controllers;

import model.entities.Note;
import model.entities.User;
import play.mvc.Http;

public class BaseControllerTest {

	private User dummyUser = null;

	protected User dummyUser() {
		if (dummyUser == null) {
			dummyUser = new User("dummy", "123", "dummy@dummy.com");
		}
		return dummyUser;
	}

	protected void setCurrentContext(User user, Http.RequestBuilder requestBuilder) {
		Http.Context ctx = new Http.Context(requestBuilder);
		if (user != null) {
			ctx.session().put("identifier", user.userName);
		}
		Http.Context.current.set(ctx);
	}
}
