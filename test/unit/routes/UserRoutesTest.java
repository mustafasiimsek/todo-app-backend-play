package unit.routes;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import controllers.UserController;
import model.services.UserService;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mock;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static play.mvc.Http.RequestBuilder;
import static play.test.Helpers.*;

public class UserRoutesTest extends BaseRoutesTest {

	@Mock
	private UserController userControllerMock;

	@Mock
	private UserService userServiceMock;

	private final int dummyReturnCode = 418;

	@Override
	protected Module getTestBindings() {
		return new AbstractModule() {
			@Override
			protected void configure() {
				bind(UserController.class).toInstance(userControllerMock);
				bind(UserService.class).toInstance(userServiceMock);

			}
		};
	}

	@Test
	public void loginActionShouldBeCalled() throws Exception {
		when(userControllerMock.login()).thenReturn(new Result(dummyReturnCode));
		//
		RequestBuilder request = new RequestBuilder()
				.uri("/a/users/login")
				.method(POST)
				.bodyJson(JsonNodeFactory.instance.nullNode());
		Result result = route(request);
		//
		verify(userControllerMock).login();
		assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void deleteActionShouldBeCalled() throws Exception {
		//create the request
		RequestBuilder request = new RequestBuilder()
				.uri("/a/users/delete")
				.method(DELETE)
				.bodyJson(JsonNodeFactory.instance.nullNode());
		//session needs an identifier
		request.session("identifier", dummyUser().userName);
		//create custom context
		Http.Context ctx = new Http.Context(request);
		//set current context as custom context
		Http.Context.current.set(ctx);
		//this stubbing is for by-passing the deadbolt
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		//this method should be called when the request is made
		when(userControllerMock.delete()).thenReturn(new Result(dummyReturnCode));
		//make the request
		Result result = route(request);
		System.out.println("result.status: " + result.status());
		verify(userControllerMock).delete();
		assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void logoutActionShouldBeCalled() throws Exception {
		when(userControllerMock.logout()).thenReturn(new Result(dummyReturnCode));
//
//        Result result =  setRequest("a/users/logout","DELETE",
//                dummyUser().userName, new Http.RequestBuilder());

		Http.RequestBuilder request = new RequestBuilder()
				.session("identifier", dummyUser().userName)
				.uri("/a/users/logout")
				.method(DELETE);
		Result result = route(request);
		verify(userControllerMock).logout();
		assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void createActionShouldBeCalled() throws Exception {
		when(userControllerMock.create()).thenReturn(new Result(dummyReturnCode));
		Http.RequestBuilder request = new RequestBuilder()
				.uri("/a/users")
				.method(POST)
				.bodyJson(Json.toJson(dummyUser()));
		Result result = route(request);
		verify(userControllerMock).create();
		assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void updateActionShouldBeCalled() throws Exception {
		when(userControllerMock.update()).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		Http.RequestBuilder request = new RequestBuilder()
				.uri("/a/users")
				.method(PUT)
				.bodyJson(Json.toJson(dummyUser()))
				.session("identifier", dummyUser().userName);
		Result result = route(request);
		verify(userControllerMock).update();
		assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void infoActionShouldBeCalled() throws Exception {
		when(userControllerMock.info()).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		Http.RequestBuilder request = new RequestBuilder()
				.uri("/a/users/info")
				.method(GET)
				.session("identifier", dummyUser().userName);
		Result result = route(request);
		verify(userControllerMock).info();
		assertEquals(dummyReturnCode, result.status());
	}

	@After
	public void tearDown() throws Exception {
		Http.Context.current.set(new Http.Context(new RequestBuilder()));
	}
}
