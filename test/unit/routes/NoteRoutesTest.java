package unit.routes;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import controllers.NoteController;
import model.entities.Note;
import model.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import play.mvc.Http;
import play.mvc.Result;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static play.test.Helpers.*;

public class NoteRoutesTest extends BaseRoutesTest {

	@Mock
	private NoteController noteControllerMock;

	@Mock
	private UserService userServiceMock;

	private final int dummyReturnCode = 418;

	private Note dummyNote = null;

	private Note dummyNote() {
		if (this.dummyNote == null) {
			this.dummyNote = new Note(1L, "dummy note", dummyUser());
			return dummyNote;
		} else {
			return this.dummyNote;
		}
	}


	@Override
	protected Module getTestBindings() {
		return new AbstractModule() {
			@Override
			protected void configure() {
				bind(NoteController.class).toInstance(noteControllerMock);
				bind(UserService.class).toInstance(userServiceMock);
			}
		};
	}

	@Test
	public void createActionShouldBeCalled() throws Exception {

		when(noteControllerMock.create()).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		Http.RequestBuilder request = new Http.RequestBuilder()
				.uri("/a/users/notes")
				.method(POST)
				.session("identifier", dummyUser().userName)
				.bodyJson(JsonNodeFactory.instance.nullNode());
		Result result = route(request);
		verify(noteControllerMock).create();
		Assert.assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void updateActionShouldBeCalled() throws Exception {
		when(noteControllerMock.update(dummyNote().id)).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyUser().userName)).thenReturn(dummyUser());
		Http.RequestBuilder request = new Http.RequestBuilder()
				.uri("/a/users/notes/" + dummyNote().id)
				.method(PUT)
				.session("identifier", dummyUser().userName)
				.bodyJson(JsonNodeFactory.instance.nullNode());
		Result result = route(request);
		verify(noteControllerMock).update(dummyNote().id);
		Assert.assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void deleteActionShouldBeCalled() throws Exception {
		when(noteControllerMock.delete(dummyNote().id)).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyNote().user.userName)).thenReturn(dummyUser());
		Http.RequestBuilder request = new Http.RequestBuilder()
				.uri("/a/users/notes/" + dummyNote().id)
				.method(DELETE)
				.session("identifier", dummyUser().userName)
				.bodyJson(JsonNodeFactory.instance.nullNode());
		Result result = route(request);
		verify(noteControllerMock).delete(dummyNote().id);
		Assert.assertEquals(dummyReturnCode, result.status());
	}

	@Test
	public void getAllNoteActionShouldBeCalled() throws Exception {
		when(noteControllerMock.getAllNote()).thenReturn(new Result(dummyReturnCode));
		when(userServiceMock.getByUsername(dummyNote().user.userName)).thenReturn(dummyNote().user);
		Http.RequestBuilder request = new Http.RequestBuilder()
				.uri("/a/users/notes")
				.method(GET)
				.session("identifier", dummyNote().user.userName);
		Result result = route(request);
		verify(noteControllerMock).getAllNote();
		Assert.assertEquals(dummyReturnCode, result.status());
	}
}
