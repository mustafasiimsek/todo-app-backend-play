package unit.routes;

import com.google.inject.Module;
import model.entities.User;
import org.mockito.MockitoAnnotations;
import play.Application;
import play.Mode;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.WithApplication;

public abstract class BaseRoutesTest extends WithApplication {

	protected abstract Module getTestBindings();

	@Override
	protected Application provideApplication() {
		MockitoAnnotations.initMocks(this);
		return new GuiceApplicationBuilder()
				.in(Mode.TEST)
				.overrides(getTestBindings())
				.build();
	}

	private User dummyUser;

	protected User dummyUser() {
		if (dummyUser == null) {
			dummyUser = new User(1L, "dummy.user", "dummyUser@click2correct.com",
					"dummyUser@click2correct.com", "Dummy", "User");
		}
		return dummyUser;
	}

//	protected Result setRequest(String uri, String method,
//							  String session, Http.RequestBuilder request){
//	    request
//                .uri(uri)
//                .method(method)
//                .session().put("identifier",session);
//	    return route(request);
//
//    }

}
