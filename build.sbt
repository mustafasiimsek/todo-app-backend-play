name := """use-play"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  javaJpa,
  "mysql" % "mysql-connector-java" % "5.1.36",
  "org.hibernate" % "hibernate-entitymanager" % "4.3.11.Final",
  "be.objectify" %% "deadbolt-java" % "2.5.4",
  "org.mockito" % "mockito-core" % "2.8.47" % "test"
)
