package security;

import be.objectify.deadbolt.java.DeadboltHandler;
import be.objectify.deadbolt.java.cache.HandlerCache;
import play.api.Configuration;
import play.api.Environment;
import play.api.inject.Binding;
import play.api.inject.Module;
import scala.collection.Seq;

import javax.inject.Singleton;

public class CustomDeadboltHook extends Module {
	@Override
	public Seq<Binding<?>> bindings(final Environment environment,
	                                final Configuration configuration) {
		return seq(bind(DeadboltHandler.class)
						.qualifiedWith(HandlerQualifiers.MainHandler.class)
						.to(DefaultDeadboltHandler.class).in(Singleton.class),
				bind(HandlerCache.class).to(MyHandlerCache.class).in(Singleton.class));
	}

}