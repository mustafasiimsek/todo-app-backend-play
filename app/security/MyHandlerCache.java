package security;

import be.objectify.deadbolt.java.DeadboltHandler;
import be.objectify.deadbolt.java.cache.HandlerCache;
import security.HandlerQualifiers;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class MyHandlerCache implements HandlerCache {
	private final DeadboltHandler handler;

	private final Map<String, DeadboltHandler> handlers = new HashMap<>();

	@Inject
	public MyHandlerCache(@HandlerQualifiers.MainHandler final DeadboltHandler handler) {
		this.handler = handler;
		this.handlers.put(handler.handlerName(),
				handler);
	}

	@Override
	public DeadboltHandler apply(final String name) {
		return handlers.get(name);
	}

	@Override
	public DeadboltHandler get() {
		return handler;
	}

}
