package security;


import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import be.objectify.deadbolt.java.ExecutionContextProvider;
import be.objectify.deadbolt.java.models.Subject;
import com.google.inject.Inject;
import model.services.UserService;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;


import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;

public class DefaultDeadboltHandler extends AbstractDeadboltHandler {

	private UserService userService;

	@Inject
	public DefaultDeadboltHandler(ExecutionContextProvider ecProvider, UserService userService) {
		super(ecProvider);
		this.userService = userService;
	}

	@Override
	public CompletionStage<Optional<Result>> beforeAuthCheck(final Http.Context context) {
		// returning null means that everything is OK.  Return a real result if you want a redirect to a login page or
		// somewhere else
		return CompletableFuture.completedFuture(Optional.empty());
	}

	@Override
	public CompletionStage<Optional<? extends Subject>> getSubject(final Http.Context context) {
		// in a real application, the user name would probably be in the session following a login process
		return CompletableFuture.supplyAsync(() ->
						Optional.ofNullable(userService.getByUsername(
								context.session().get("identifier"))),
				(Executor) executionContextProvider.get()
		);
	}

	@Override
	public CompletionStage<Result> onAuthFailure(final Http.Context context,
	                                             final Optional<String> content) {
		return CompletableFuture.completedFuture(unauthorized("Not allowed."));
		// başka bir sayfaya yönlendirmek için
		// return CompletableFuture.completedFuture(redirect("/a/home"));

	}
}

