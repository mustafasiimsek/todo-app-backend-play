package controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class HomeController extends Controller {

	public Result home() {
		if (session("identifier") != null)
			return ok("Welcome " + session("identifier"));
		else
			return ok("Welcome");
	}
}
