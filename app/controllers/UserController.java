package controllers;

import be.objectify.deadbolt.java.actions.SubjectNotPresent;
import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.google.inject.Inject;
import model.entities.User;
import model.services.UserService;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class UserController extends Controller {

	private UserService userService;

	@Inject
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@Transactional
	@SubjectNotPresent
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() {
		User userFromReq = Json.fromJson(request().body().asJson(), User.class);
		return ok(Json.toJson(userService.create(userFromReq)));
	}

	@Transactional
	@SubjectPresent
	public Result update() {
		User userFromReq = Json.fromJson(request().body().asJson(), User.class);
		return ok(Json.toJson(userService.update(session("identifier"), userFromReq)));
	}

	@Transactional
	@SubjectPresent
	public Result delete() {
		userService.delete(session("identifier"));
		session().clear();
		return ok();
	}

	@Transactional
	@SubjectNotPresent
	public Result login() {
		String userName = null;
		User userFromReq = Json.fromJson(request().body().asJson(), User.class);
		userName = userService.checkPassword(userFromReq);
		if (userName != null) {
			session().put("identifier", userName);
			return ok("welcome " + userName);
		} else {
			return redirect("/a/users/login");
		}
	}

	public Result logout() {
		session().clear();
		return ok("Bye.");
	}

	@Transactional
	@SubjectPresent
	public Result info() {
		return ok(Json.toJson(userService.getByUsername(session("identifier"))));
	}

}
