package controllers;

import be.objectify.deadbolt.java.actions.SubjectPresent;
import com.google.inject.Inject;
import model.entities.Note;
import model.services.NoteService;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

public class NoteController extends Controller {

	private NoteService noteService;

	@Inject
	public NoteController(NoteService noteService) {
		this.noteService = noteService;
	}

	@Transactional
	@SubjectPresent
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() {
		String userName = session().get("identifier");
		Note noteFromReq = Json.fromJson(request().body().asJson(), Note.class);
		return ok(Json.toJson(noteService.create(userName, noteFromReq)));
	}

	@Transactional
	@SubjectPresent
	public Result delete(Long id) {
		if (session("identifier").equals(noteService.getById(id).user.userName)) {
			noteService.delete(id);
			return ok();
		} else {
			return unauthorized("Not allowed.");
		}
	}

	@Transactional
	@SubjectPresent
	public Result getAllNote() {
		int page = Integer.parseInt(request().getQueryString("page"));
		if (page > 0) {
			return ok(Json.toJson(noteService.getAllNote(session("identifier"), page)));
		} else {
			return unauthorized("Not allowed.");
		}
	}

	@Transactional
	@SubjectPresent
	public Result update(Long id) {
		Note noteFromReq = Json.fromJson(request().body().asJson(), Note.class);
//        return ok(Json.toJson(noteService
//                .update(session("identifier"),id, noteFromReq)));
		if (session("identifier").equals(noteService.getById(id).user.userName)) {
			return ok(Json.toJson(noteService
					.update(id, noteFromReq)));
		} else {
			return unauthorized("Not allowed.");
		}
	}
}
