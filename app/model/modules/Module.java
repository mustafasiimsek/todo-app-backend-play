package model.modules;

import com.google.inject.AbstractModule;
import model.repositories.NoteRepository;
import model.repositories.UserRepository;
import model.repositories.jpql_impl.NoteRepositoryJPAImpl;
import model.repositories.jpql_impl.UserRepositoryJPAImpl;

import java.time.Clock;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the Play
 * application starts.
 * <p>
 * Play will automatically use any class called `model.Module` that is in
 * the root package. You can create modules in other locations by
 * adding `play.modules.enabled` settings to the `application.conf`
 * configuration file.
 */
public class Module extends AbstractModule {

	@Override
	public void configure() {

		// Use the system clock as the default implementation of Clock
		bind(Clock.class).toInstance(Clock.systemDefaultZone());
		// Ask Guice to create an instance of ApplicationTimer when the
		// application starts.

		//NORMALDE KULLANILAN
		//    bind(ObjectRepository.class).to( NoteRepositoryJPAImpl.class);

//        bind(ApplicationTimer.class).asEagerSingleton();
		//       bind(Counter.class).to(AtomicCounter.class);


		//GENERİC TYPE KULLANDIĞIMIZDA KULLANILAN
		//İKİSİDE GENERİC İSE İKİSİNİDE AYNI ŞEKİLDE YAP
		//İMPORTU KENDİN YAP SIKINTI ÇIKARABİLİYOR

//        bind(new TypeLiteral<ObjectRepository<User>>(){})
//                .to(NoteRepositoryJPAImpl.class);

		bind(UserRepository.class).to(UserRepositoryJPAImpl.class);
		bind(NoteRepository.class).to(NoteRepositoryJPAImpl.class);
	}

}
