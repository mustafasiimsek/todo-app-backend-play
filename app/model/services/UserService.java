package model.services;

import com.google.inject.Inject;
import model.entities.User;
import model.repositories.UserRepository;
import play.db.jpa.Transactional;

public class UserService {

	private UserRepository userRepository;

	@Inject
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public User create(User user) {
		return userRepository.create(user);
	}

	public User update(String userName, User user) {
		user.userName = userName;
		return userRepository.update(user);
	}

	public void delete(String userName) {
		userRepository.delete(userName);
	}


	public String checkPassword(User user) {
		return userRepository.checkPassword(user);
	}

	public User getByUsername(String userName) {
		return userRepository.getByUsername((userName));
	}
}
