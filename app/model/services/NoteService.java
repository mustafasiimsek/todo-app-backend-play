package model.services;

import com.google.inject.Inject;
import model.entities.Note;
import model.repositories.NoteRepository;

import javax.annotation.Nullable;
import java.util.List;

public class NoteService {

	private NoteRepository noteRepository;

	@Inject
	public NoteService(NoteRepository noteRepository) {
		this.noteRepository = noteRepository;
	}

	public Note create(String userName, Note note) {
		return noteRepository.create(userName, note);
	}

	public void delete(Long id) {
		noteRepository.delete(id);
	}

	public List<Note> getAllNote(String userName, int page) {
		return noteRepository.getAllNote(userName, page);
	}

	public Note update(Long id, Note note) {
		note.id = id;
		return noteRepository.update(note);
	}

	public Note getById(Long id) {
		return noteRepository.getById(id);
	}
}