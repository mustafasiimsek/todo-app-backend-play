package model.repositories;

import model.entities.Note;

import javax.annotation.Nonnull;
import java.util.List;

public interface NoteRepository {

	Note getById(@Nonnull Long id);

	void delete(@Nonnull Long id);

	Note create(@Nonnull String userName, @Nonnull Note note);

	Note update(@Nonnull Note note);

	List<Note> getAllNote(@Nonnull String userName, int page);
}
