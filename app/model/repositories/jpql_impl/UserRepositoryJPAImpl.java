package model.repositories.jpql_impl;

import com.google.inject.Inject;
import model.entities.User;
import model.repositories.UserRepository;
import play.db.jpa.JPAApi;

import javax.annotation.Nonnull;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


public class UserRepositoryJPAImpl implements UserRepository {

	private JPAApi jpaApi;

	@Inject
	public UserRepositoryJPAImpl(JPAApi jpaApi) {
		this.jpaApi = jpaApi;
	}

	@Override
	public User create(@Nonnull User user) {
		jpaApi.em().persist(user);
		return user;
	}

	@Override
	public User update(@Nonnull User user) {
		user.id = getByUsername(user.userName).id;
		return jpaApi.em().merge(user);
	}

	@Override
	public User getByUsername(@Nonnull String userName) {
		String q = "Select u from User u Where userName = :userName";
		TypedQuery<User> userTypedQuery = jpaApi.em()
				.createQuery(q, User.class)
				.setParameter("userName", userName);
		try {
			return userTypedQuery.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public void delete(@Nonnull String userName) {

		jpaApi.em().remove(getByUsername(userName));
	}

	@Override
	public String checkPassword(@Nonnull User user) {
		String password = user.password;
		String q = "Select u from User u Where userName = :userName OR email =:email ";
		TypedQuery<User> userTypedQuery = jpaApi.em()
				.createQuery(q, User.class)
				.setParameter("userName", user.userName)
				.setParameter("email", user.email);
		user = userTypedQuery.getSingleResult();
		if (user.password.equals(password)) {
			return user.userName;

		} else {
			return null;
		}
	}

}
