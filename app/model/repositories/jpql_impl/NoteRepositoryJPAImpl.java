package model.repositories.jpql_impl;

import com.google.inject.Inject;
import model.entities.Note;
import model.entities.User;
import model.repositories.NoteRepository;
import play.db.jpa.JPAApi;

import javax.annotation.Nonnull;
import javax.persistence.TypedQuery;
import java.util.List;

public class NoteRepositoryJPAImpl implements NoteRepository {

	private JPAApi jpaApi;

	@Inject
	public NoteRepositoryJPAImpl(JPAApi jpaApi) {
		this.jpaApi = jpaApi;
	}

	@Override
	public Note getById(@Nonnull Long id) {
		return jpaApi.em().find(Note.class, id);
	}

	@Override
	public void delete(@Nonnull Long id) {
		jpaApi.em().remove(jpaApi.em().find(Note.class, id));
	}

	@Override
	public Note create(@Nonnull String userName, @Nonnull Note note) {
		note.user = getUserByUsername(userName);
		jpaApi.em().persist(note);
		return note;
	}

	@Override
	public Note update(@Nonnull Note note) {
		jpaApi.em().find(Note.class, note.id).note = note.note;
		//note.user = jpaApi.em().find(Note.class,note.id).user;
		return jpaApi.em().find(Note.class, note.id);
	}

	@Override
	public List<Note> getAllNote(@Nonnull String userName, int page) {
		Long user_id = getUserByUsername(userName).id;
		String q = "Select n from Note n Where user_id = :user_id";
		TypedQuery<Note> noteTypedQuery = jpaApi.em()
				.createQuery(q, Note.class).setParameter("user_id", user_id);
		noteTypedQuery.setFirstResult((page - 1) * 5);
		noteTypedQuery.setMaxResults(page * 5);
		return noteTypedQuery.getResultList();
	}

	private User getUserByUsername(@Nonnull String userName) {
		String q = "Select u from User u Where userName = :userName";
		TypedQuery<User> userTypedQuery = jpaApi.em()
				.createQuery(q, User.class).setParameter("userName", userName);
		return userTypedQuery.getSingleResult();

	}
}

