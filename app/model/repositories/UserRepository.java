package model.repositories;

import model.entities.User;

import javax.annotation.Nonnull;

public interface UserRepository {
	User getByUsername(@Nonnull String userName);

	void delete(@Nonnull String userName);

	User create(@Nonnull User user);

	String checkPassword(@Nonnull User user);

	User update(@Nonnull User user);
}
