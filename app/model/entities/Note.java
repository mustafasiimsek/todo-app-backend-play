package model.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Target;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class Note {

	@Id
	@GeneratedValue
	public Long id;

	public String note;

	@ManyToOne
	public User user;

	public Note(Long id, String note, User user) {
		this.note = note;
		this.user = user;
		this.id = id;
	}

	public Note() {
	}
}
