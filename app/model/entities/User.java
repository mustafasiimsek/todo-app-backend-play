package model.entities;

import be.objectify.deadbolt.java.models.Permission;
import be.objectify.deadbolt.java.models.Role;
import be.objectify.deadbolt.java.models.Subject;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class User implements Subject {

	public User(String userName, String password, String email) {
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	public User(Long id, String userName, String password, String email, String firstName, String lastName) {
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public User() {
	}

	@Id
	@GeneratedValue
	public Long id;

	@NotNull
	@Column(unique = true)
	public String userName;

	@NotNull
	public String password;

	@NotNull
	@Column(unique = true)
	public String email;

	@Column(name = "first_name")
	public String firstName;

	@Column(name = "last_name")
	public String lastName;

//    @ManyToMany
//    private List<SecurityRole> roles;
//
//    @ManyToMany
//    private List<UserPermission> permissions;

	@Override
	public List<? extends Role> getRoles() {
		return null;
	}

	@Override
	public List<? extends Permission> getPermissions() {
		return null;
	}

	@Override
	public String getIdentifier() {
		return userName;
	}
}
